user = FactoryGirl.create :user, first_name: "Giannicola", last_name: "Olivadoti", email: "olinicola@gmail.com", ruzzes_count: 1

Dir.glob(File.join(Rails.root, "spec", "support", "images", "*.jpg")).each do |image|
  file = Rack::Test::UploadedFile.new(image, "image/jpeg")
  FactoryGirl.create :image_with_ruzz, asset: file
end

FactoryGirl.create :like, ruzz: Ruzz.ordered_by_creation.first, user: user
