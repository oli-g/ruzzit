class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :asset,          null: false, default: ""
      t.string :content_type,   null: false, default: ""
      t.integer :size,          null: false, default: 0

      t.timestamps
    end
  end
end
