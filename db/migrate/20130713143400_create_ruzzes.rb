class CreateRuzzes < ActiveRecord::Migration
  def change
    create_table :ruzzes do |t|
      t.string :name,           null: false, default: ""
      t.text :description
      t.references :user
      t.references :attachable, polymorphic: true

      t.timestamps
    end

    add_index :ruzzes, :created_at
    add_index :ruzzes, :user_id
    add_index :ruzzes, [:attachable_id, :attachable_type]
  end
end
