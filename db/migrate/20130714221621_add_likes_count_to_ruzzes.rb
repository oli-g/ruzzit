class AddLikesCountToRuzzes < ActiveRecord::Migration
  def self.up
    add_column :ruzzes, :likes_count, :integer, null:false, default: 0, after: :description

    # Reset likes counter cache
    Ruzz.select(:id).each do |ruzz|
      Ruzz.reset_counters(ruzz.id, :likes)
    end
  end

  def self.down
    remove_column :ruzzes, :likes_count
  end
end
