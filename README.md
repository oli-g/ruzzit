RuzziT
======

Rails 4 clone of Dribble.

Development
-----------

    # Put S3 credentials in config/application.yml
    bundle exec rake db:create
    bundle exec rake db:migrate
    bundle exec rake db:seed
    

Test
----

    bundle exec rake db:test:prepare
    bundle exec guard

Deployment
----------

    heroku config:set AWS_ACCESS_KEY_ID="XXX"
    heroku config:set AWS_SECRET_ACCESS_KEY="YYY"
    heroku config:set S3_BUCKET_NAME="ZZZZ"
    git push heroku master    
