namespace :carrierwave do
  desc "Recreate Images versions from the base file, re-uploading and processing them"
  task recreate_versions: :environment do
    Image.all.each do |image|
      puts "Recreating versions of Image #{image.asset.url}..."
      image.asset.recreate_versions!
    end
  end
end
