require "spec_helper"

describe User do
  context "associations" do
    it { should have_many :ruzzes }
    it { should have_many :likes }
  end

  context "validations" do
    it { should validate_presence_of :first_name }
    it { should validate_presence_of :last_name }
  end

  context "methods" do
    describe "#full_name" do
      subject(:user) { create :user }
      its(:full_name) { should eq "#{user.first_name} #{user.last_name}" }
    end
  end
end
