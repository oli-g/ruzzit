require "spec_helper"

describe Ruzz do
  context "associations" do
    it { should belong_to :user }
    it { should belong_to :attachable }
    it { should have_many :likes }
  end

  context "validations" do
    it { should validate_presence_of :name }
    it { should validate_presence_of :description }
  end

  context "scopes" do
    describe ".ordered_by_creation" do
      before { create_list :ruzz, 10 }
      subject(:ruzzes) { Ruzz.ordered_by_creation }
      it { expect(ruzzes.first.created_at).to be >= ruzzes.last.created_at }
    end
  end

  context "methods" do
    describe "#image_url" do
      subject(:ruzz) { create :ruzz }
      it { expect(ruzz.image_url).to eq ruzz.attachable.asset_url }
    end

    describe "#thumb_url" do
      subject(:ruzz) { create :ruzz }
      it { expect(ruzz.thumb_url).to eq ruzz.attachable.asset_url(:thumb) }
    end
  end
end
