require "spec_helper"

describe Like do
  context "associations" do
    it { should belong_to :ruzz }
    it { should belong_to :user }

    context "counter cache" do
      let!(:ruzz) { create :ruzz, likes_count: 5 }
      let!(:user) { create :jon_doe_user}
      it { expect(ruzz.reload.likes_count).to eq 5 }
      it { ruzz.likes.first.destroy!; expect(ruzz.reload.likes_count).to eq 4 }
      it { ruzz.likes.create(user: user); expect(ruzz.reload.likes_count).to eq 6 }
    end
  end
end
