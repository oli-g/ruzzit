require "spec_helper"

describe Image do
  context "associations" do
    it { should have_one :ruzz }
  end

  context "validations" do
    it { should validate_presence_of :asset }
  end
end
