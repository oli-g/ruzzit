FactoryGirl.define do
  factory :ruzz do
    name        { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    user
    association :attachable, factory: :image

    ignore do
      likes_count 0
    end

    after :create do |ruzz, evaluator|
      FactoryGirl.create_list :like, evaluator.likes_count, ruzz: ruzz
    end
  end
end
