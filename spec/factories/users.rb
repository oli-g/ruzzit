FactoryGirl.define do
  factory :user do
    first_name            { Faker::Name.first_name }
    last_name             { Faker::Name.last_name }
    email                 { Faker::Internet.email }
    password              "changeme"
    password_confirmation "changeme"

    ignore do
      likes_count  0
      ruzzes_count 0
    end

    after :create do |user, evaluator|
      FactoryGirl.create_list :like, evaluator.likes_count, user: user
      FactoryGirl.create_list :ruzz, evaluator.ruzzes_count, user: user
    end

    factory :jon_doe_user do
      first_name            "Jon"
      last_name             "Doe"
      email                 "jon@doe.com"
      password              "changeme"
      password_confirmation "changeme"
    end
  end
end
