FactoryGirl.define do
  factory :image do
    images_folder = File.join(Rails.root, "spec", "support", "images")

    asset { Rack::Test::UploadedFile.new(Dir.glob(File.join(images_folder, "*.jpg")).sample, "image/jpeg") }
    ruzz  nil

    factory :image_with_ruzz do
      after :create do |image, evaluator|
        FactoryGirl.create :ruzz, attachable: image
      end
    end
  end
end
