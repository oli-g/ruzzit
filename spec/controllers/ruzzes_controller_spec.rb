require "spec_helper"

describe RuzzesController do
  describe "GET #index" do
    before(:all) { create_list :ruzz, 30 }

    it "populates an array of ruzzes" do
      ruzzes = Ruzz.ordered_by_creation.page(1)
      get :index
      assigns(:ruzzes).should eq(ruzzes)

      ruzzes = Ruzz.ordered_by_creation.page(2)
      get :index, page: 2
      assigns(:ruzzes).should eq(ruzzes)
    end

    it "renders the :index view" do
      get :index
      response.should render_template :index
    end
  end
end
