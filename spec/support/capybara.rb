Capybara.configure do |config|
  config.server_port = 31234
  config.always_include_port = true

  config.javascript_driver = :poltergeist
  # config.register_driver :poltergeist do |app|
  #   Capybara::Poltergeist::Driver.new(app, debug: true)
  # end
end

RSpec.configure do |config|
  config.before(:each, js: true) do
    default_url_options[:host] = "localhost"
    Capybara.app_host = "http://localhost"
  end
end
