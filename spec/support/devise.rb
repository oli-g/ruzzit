module DeviseHelperMethods
  include Warden::Test::Helpers

  def signin(user)
    login_as(user, scope: :user)
  end

  def signout(user)
    logout(:user)
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, type: :controller
  config.include DeviseHelperMethods, type: :feature

  config.before(:each, type: [:request, :feature]) do
    Warden.test_mode!
  end

  config.after(:each, type: [:request, :feature]) do
    Warden.test_reset!
  end
end
