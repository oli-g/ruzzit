require "spec_helper"

feature "Like a ruzz", js: true do
  background do
    create_list :ruzz, 30
  end

  given!(:user) { create :jon_doe_user }

  scenario "Signed in user" do
    signin(user)
    visit root_url

    ruzz = Ruzz.ordered_by_creation.page(1).first
    old_likes_count = find_likes_count(ruzz)
    within(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']") do
      click_link "Like!"
    end

    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_link "Unlike!"
    new_likes_count = find_likes_count(ruzz)
    new_likes_count.should eq (old_likes_count + 1)
  end

  def find_likes_count(ruzz)
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").find(".likes_count").text.to_i
  end
end
