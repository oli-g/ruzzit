require "spec_helper"

feature "Sign in" do
  background do
    create :jon_doe_user
  end

  scenario "Valid credentials" do
    visit root_url
    click_link "Sign in"

    current_url.should eq new_user_session_url

    fill_in "Email", with: "jon@doe.com"
    fill_in "Password", with: "changeme"
    click_button "Sign in"

    page.should have_content "Signed in successfully."
    page.should have_link "Account"
    page.should have_link "Sign out"
    current_url.should eq root_url
  end

  scenario "Invalid credentials" do
    visit root_url
    click_link "Sign in"

    current_url.should eq new_user_session_url

    fill_in "Email", with: "invalid@credentials.com"
    fill_in "Password", with: "changeme"
    click_button "Sign in"

    page.should have_content "Invalid email or password."
    page.should have_link "Sign up"
    page.should have_link "Sign in"
    current_url.should eq new_user_session_url
  end
end
