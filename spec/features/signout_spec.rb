require "spec_helper"

feature "Sign out" do
  background do
    user = create :jon_doe_user
    signin(user)
  end

  scenario "Signed in user" do
    visit root_url
    click_link "Sign out"

    page.should have_content "Signed out successfully."
    page.should have_link "Sign up"
    page.should have_link "Sign in"
    current_url.should eq root_url
  end
end
