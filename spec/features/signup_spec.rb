require "spec_helper"

feature "Sign up" do
  scenario "Valid credentials" do
    visit root_url
    click_link "Sign up"

    current_url.should eq new_user_registration_url

    fill_in "Email", with: "jon@doe.com"
    fill_in "First name", with: "Jon"
    fill_in "Last name", with: "Doe"
    fill_in "user_password", with: "changeme"
    fill_in "user_password_confirmation", with: "changeme"
    click_button "Sign up"

    page.should have_content "Welcome! You have signed up successfully."
    page.should have_link "Account"
    page.should have_link "Sign out"
    current_url.should eq root_url
  end

  scenario "Invalid credentials" do
    visit root_url
    click_link "Sign up"

    current_url.should eq new_user_registration_url

    fill_in "Email", with: "jon@doe"
    fill_in "First name", with: "Jon"
    fill_in "Last name", with: ""
    fill_in "user_password", with: "changeme"
    fill_in "user_password_confirmation", with: "different"

    click_button "Sign up"

    page.should have_content "Please review the problems below:"
    page.should have_link "Sign up"
    page.should have_link "Sign in"
    current_url.should eq new_user_registration_url
  end
end
