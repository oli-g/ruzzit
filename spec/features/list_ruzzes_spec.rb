require "spec_helper"

feature "List ruzzes" do
  background do
    create_list :ruzz, 30
  end

  given!(:user) { create :jon_doe_user }

  scenario "Signed in user" do
    signin(user)
    create_list :like, 5, user: user

    visit root_url
    page.should have_content "RuzziT"

    Ruzz.ordered_by_creation.page(1).each do |ruzz|
      should_have_ruzz(ruzz)
      should_have_ruzz_like_or_unlike_link(ruzz, user)
    end

    visit root_url(page: 3)
    page.should have_content "RuzziT"

    Ruzz.ordered_by_creation.page(3).each do |ruzz|
      should_have_ruzz(ruzz)
      should_have_ruzz_like_or_unlike_link(ruzz, user)
    end
  end

  scenario "Signed out user" do
    visit root_url
    page.should have_content "RuzziT"

    Ruzz.ordered_by_creation.page(1).each do |ruzz|
      should_have_ruzz(ruzz)
      should_not_have_ruzz_like_and_unlike_link(ruzz)
    end

    visit root_url(page: 3)
    page.should have_content "RuzziT"

    Ruzz.ordered_by_creation.page(3).each do |ruzz|
      should_have_ruzz(ruzz)
      should_not_have_ruzz_like_and_unlike_link(ruzz)
    end
  end

  def should_have_ruzz(ruzz)
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_content ruzz.user.full_name
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_content ruzz.likes_count
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_xpath ".//img[@src='#{ruzz.thumb_url}']"
  end

  def should_have_ruzz_like_or_unlike_link(ruzz, user)
    if ruzz.liked_by?(user)
      find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_link "Unlike!"
    else
      find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should have_link "Like!"
    end
  end

  def should_not_have_ruzz_like_and_unlike_link(ruzz)
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should_not have_link "Like!"
    find(:xpath, "//li[@data-id='ruzz_#{ruzz.id}']").should_not have_link "Unlike!"
  end
end
