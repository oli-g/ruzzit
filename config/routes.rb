Ruzzit::Application.routes.draw do
  devise_for :users, skip: [:sessions, :registrations]
  as :user do
    get     "signup"  => "devise/registrations#new",     as: :new_user_registration
    post    "signup"  => "devise/registrations#create",  as: :user_registration
    get     "signin"  => "devise/sessions#new",          as: :new_user_session
    post    "signin"  => "devise/sessions#create",       as: :user_session
    delete  "signout" => "devise/sessions#destroy",      as: :destroy_user_session
    get     "account" => "devise/registrations#edit",    as: :edit_user_registration
    patch   "account" => "devise/registrations#update",  as: ""
    put     "account" => "devise/registrations#update",  as: ""
    delete  "account" => "devise/registrations#destroy", as: ""
  end

  resources :ruzzes, only: [:index, :new, :create] do
    resources :likes, only: [:create, :destroy]
  end

  root "ruzzes#index"
end
