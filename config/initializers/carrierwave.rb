CarrierWave.configure do |config|
  if Rails.env.development? or Rails.env.production?
    config.cache_dir                        = "#{Rails.root}/tmp/uploads"
    config.storage                          = :aws
    config.aws_bucket                       = ENV["S3_BUCKET_NAME"]
    config.aws_acl                          = :public_read
    config.asset_host                       = "#{ENV["S3_BUCKET_NAME"]}.s3-website-eu-west-1.amazonaws.com"
    config.aws_authenticated_url_expiration = 60 * 60 * 24 * 365

    config.aws_credentials = {
      access_key_id:     ENV["AWS_ACCESS_KEY_ID"],
      secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
    }
  else
    config.storage           = :file
    config.enable_processing = false if Rails.env.test?
  end
end
