//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require bootstrap
//= require boot-business

$(document).ready ->
  $("form.new_like a, form.edit_like a").on "click", (event) ->
    event.preventDefault()
    $(this).closest("form").submit()
