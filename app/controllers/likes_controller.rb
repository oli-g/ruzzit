class LikesController < ApplicationController
  respond_to :js

  before_action :authenticate_user!

  def create
    @like = Like.create(like_params)
    @ruzz = Ruzz.find(like_params[:ruzz_id])
    render :toggle
  end

  def destroy
    @like = Like.find(params[:id])
    @like.destroy!
    @ruzz = Ruzz.find(params[:ruzz_id])
    render :toggle
  end

private

  def like_params
    params.require(:like).permit(:ruzz_id, :user_id)
  end
end
