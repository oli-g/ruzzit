class RuzzesController < ApplicationController
  respond_to :html

  before_filter :authenticate_user!, except: :index

  def index
    @ruzzes = Ruzz.ordered_by_creation.page(params[:page])
  end

  def new
    @image = Image.new
    @ruzz = @image.build_ruzz(user_id: current_user.id)
  end

  def create
    @image = Image.new(image_params)
    @ruzz = @image.ruzz
    if @image.save
      redirect_to ruzzes_path
    else
      render :new
    end
  end

private

  def image_params
    params.require(:image).permit(
      :asset, :asset_cache, ruzz_attributes: [:name, :description, :user_id]
    )
  end
end
