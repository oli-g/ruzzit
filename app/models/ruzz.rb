class Ruzz < ActiveRecord::Base
  validates :name, presence: true
  validates :description, presence: true

  belongs_to :user, touch: true
  belongs_to :attachable, polymorphic: true
  has_many :likes, dependent: :destroy

  scope :ordered_by_creation, ->(order = "DESC") do
    order("created_at #{order.upcase}")
  end

  def image_url
    attachable.asset_url unless attachable.nil?
  end

  def thumb_url
    attachable.asset_url(:thumb) unless attachable.nil?
  end

  def liked_by?(user)
    likes.where(user: user).first.present?
  end
end
