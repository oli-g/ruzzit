class Image < ActiveRecord::Base
  mount_uploader :asset, ImageUploader

  validates :asset, presence: true, integrity: true, processing: true

  before_save :update_asset_attributes

  has_one :ruzz, as: :attachable, dependent: :destroy
  accepts_nested_attributes_for :ruzz

private

  def update_asset_attributes
    if asset.present? && asset_changed?
      content_type = asset.file.content_type
      size = asset.file.size
    end
  end
end
